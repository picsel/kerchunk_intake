# kerchunk_intake

This notebook explains how the use of [auto-]kerchunk and intake packages allows to get access to huge datasets, easily and very quickly. 

Then some tests are performed to explore the impacti of the chunk and give an insight for the storage of model outputs.


## Get the notebook

git clone https://gitlab.ifremer.fr/picsel/kerchunk_intake.git
git pull
```

